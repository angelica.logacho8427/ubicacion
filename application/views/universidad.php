<br>
<h1>Direccion de la UTC</h1>
<img src="<?php echo base_url('assets/img/logo.png'); ?>" alt="Logo UTC">
<div id="mapa1" style="width: 100%; height: 300px; border: 2px solid black;">

</div>
<script type="text/javascript">
   function initMap(){

      var coordenadacentral=new google.maps.LatLng(-0.9176727949585488, -78.63287851512106);
      var miMapa=new google.maps.Map(
        document.getElementById('mapa1'),
        {
          center: coordenadacentral,
          zoom: 9,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
      );
   var marcadorUTC=
   new google.maps.Marker({
     position: new google.maps.LatLng(-0.9176298851192035, -78.63276049792167),
     map: miMapa,
     title: 'UTC Matriz',
     icon:'<?php echo base_url('assets/img/logo.png'); ?>'
      });

      var marcadorSalache=
      new google.maps.Marker({
        position: new google.maps.LatLng(-0.9992881909255148, -78.61851513097783),
        map: miMapa,
        title: 'Campus Salache'
         });

         var marcadorPujili=
         new google.maps.Marker({
           position: new google.maps.LatLng(-0.9565290197196367, -78.689519475735),
           map: miMapa,
           title: 'Extension Pujili'
            });

            var marcadorMana=
            new google.maps.Marker({
              position: new google.maps.LatLng(-0.9117261938473397, -79.23400631389035),
              map: miMapa,
              title: 'Extension La Mana',
              icon:'<?php echo base_url('assets/img/logo.png'); ?>'
               });
   }
</script>
